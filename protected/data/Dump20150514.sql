CREATE DATABASE  IF NOT EXISTS `eventosadv2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `eventosadv2`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eventosadv2
-- ------------------------------------------------------
-- Server version	5.6.24-0ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Transferido`
--

DROP TABLE IF EXISTS `Transferido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transferido` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `transferido` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Transferido`
--

LOCK TABLES `Transferido` WRITE;
/*!40000 ALTER TABLE `Transferido` DISABLE KEYS */;
INSERT INTO `Transferido` VALUES (0,'NO'),(3,'UES Servicios Ambulatorios'),(4,'UES Urgencias'),(5,'UES Neurocirugia'),(6,'UES Pediatria'),(7,'UES Ginecologia y Obstetricia'),(8,'UES Medicina Intena'),(9,'UES Quirugicas'),(10,'UES Terapia intensiva'),(11,'UES Sala de operaciones'),(12,'UES Pensionados'),(13,'UES Ortopedia'),(14,'UES Endoscopia'),(15,'UES Unidad salud mental'),(16,'UES Imágenes diagnosticas'),(17,'UES Laboratorio clinico'),(18,'UES Banco de sangre'),(19,'Otra dependencia');
/*!40000 ALTER TABLE `Transferido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clasificacion`
--

DROP TABLE IF EXISTS `clasificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clasificacion` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `clasificacion` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasificacion`
--

LOCK TABLES `clasificacion` WRITE;
/*!40000 ALTER TABLE `clasificacion` DISABLE KEYS */;
INSERT INTO `clasificacion` VALUES (0,'Seleccione'),(1,'Leve'),(2,'Moderado'),(3,'Grave');
/*!40000 ALTER TABLE `clasificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencia`
--

DROP TABLE IF EXISTS `dependencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencia` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `dependencia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencia`
--

LOCK TABLES `dependencia` WRITE;
/*!40000 ALTER TABLE `dependencia` DISABLE KEYS */;
INSERT INTO `dependencia` VALUES (1,'UES Servicios Ambulatorios'),(2,'UES Urgencias'),(3,'UES Neurocirugia'),(4,'UES Pediatria'),(5,'UES Ginecologia y Obstetricia'),(6,'UES Medicina Intena'),(7,'UES Cirugia'),(8,'UES Terapia intensiva'),(9,'UES Sala de operaciones'),(10,'UES Hemato-Oncologia'),(11,'UES Ortopedia'),(12,'UES Endoscopia'),(13,'UES Unidad salud mental'),(14,'UES Ayudas diagnosticas'),(15,'UES Laboratorio clinico'),(16,'UES Banco de sangre'),(17,'Central Esterilizacion'),(18,'UES Medicina Física y Rehabilitacion'),(19,'UES Trasplantes'),(20,'Servicio Farmaceutico'),(26,'Otra dependencia');
/*!40000 ALTER TABLE `dependencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `edad`
--

DROP TABLE IF EXISTS `edad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edad` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `edad` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `edad`
--

LOCK TABLES `edad` WRITE;
/*!40000 ALTER TABLE `edad` DISABLE KEYS */;
INSERT INTO `edad` VALUES (1,'Menor de 1'),(2,'1-4'),(3,'4-9'),(4,'10-14'),(5,'15-19'),(6,'20-24'),(7,'25-29'),(8,'30-34'),(9,'35-39'),(10,'40-44'),(11,'45-49'),(12,'50-54'),(13,'55-59'),(14,'60-64'),(15,'65-69'),(16,'70-74'),(17,'Mayor 74');
/*!40000 ALTER TABLE `edad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventoadv`
--

DROP TABLE IF EXISTS `eventoadv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventoadv` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `historia` bigint(15) NOT NULL,
  `edad` int(8) NOT NULL,
  `fechareporte` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaocurrencia` date NOT NULL,
  `hora` time NOT NULL,
  `iddependencia` int(8) NOT NULL,
  `idreportante` int(8) NOT NULL,
  `idlugaro` int(8) NOT NULL,
  `idtipoevento` int(8) NOT NULL,
  `idincidente` int(8) NOT NULL,
  `idtipoeventoadv` int(8) NOT NULL,
  `idtransferido` int(8) NOT NULL,
  `idfactoresmedioambiente` int(8) NOT NULL,
  `idfactoresadmon` int(8) NOT NULL,
  `idfactoresrh` int(8) NOT NULL,
  `idfactorestec` int(8) NOT NULL,
  `idfactortarea` int(8) NOT NULL,
  `idfactorespaciente` int(8) NOT NULL,
  `gestionado` int(8) NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL,
  `idclasificacion` int(8) NOT NULL,
  `ideventoadvtmp` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `edad` (`edad`),
  KEY `iddependencia` (`iddependencia`,`idreportante`,`idlugaro`),
  KEY `idincidente` (`idincidente`,`idtipoeventoadv`,`idtransferido`,`idfactoresmedioambiente`,`idfactoresadmon`),
  KEY `idfactoresrh` (`idfactoresrh`,`idfactorestec`,`idfactortarea`),
  KEY `idfactorespaciente` (`idfactorespaciente`),
  KEY `idclasificacion` (`idclasificacion`),
  KEY `idreportante` (`idreportante`),
  KEY `idlugaro` (`idlugaro`),
  KEY `idtipoevento` (`idtipoevento`),
  KEY `idtipoeventoadv` (`idtipoeventoadv`),
  KEY `idtransferido` (`idtransferido`),
  KEY `idfactoresmedioambiente` (`idfactoresmedioambiente`),
  KEY `idfactoresadmon` (`idfactoresadmon`),
  KEY `idfactorestec` (`idfactorestec`),
  KEY `idfactortarea` (`idfactortarea`),
  KEY `gestionado` (`gestionado`),
  KEY `aventoadv_ibfk_16_idx` (`ideventoadvtmp`),
  CONSTRAINT `aventoadv_ibfk_16` FOREIGN KEY (`ideventoadvtmp`) REFERENCES `eventoadvtmp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `eventoadv_ibfk_1` FOREIGN KEY (`iddependencia`) REFERENCES `dependencia` (`id`),
  CONSTRAINT `eventoadv_ibfk_10` FOREIGN KEY (`idfactoresrh`) REFERENCES `factoresrh` (`id`),
  CONSTRAINT `eventoadv_ibfk_11` FOREIGN KEY (`idfactorestec`) REFERENCES `factorestec` (`id`),
  CONSTRAINT `eventoadv_ibfk_12` FOREIGN KEY (`idfactortarea`) REFERENCES `factorestarea` (`id`),
  CONSTRAINT `eventoadv_ibfk_13` FOREIGN KEY (`idfactorespaciente`) REFERENCES `factorespaciente` (`id`),
  CONSTRAINT `eventoadv_ibfk_14` FOREIGN KEY (`gestionado`) REFERENCES `gestionado` (`id`),
  CONSTRAINT `eventoadv_ibfk_15` FOREIGN KEY (`idclasificacion`) REFERENCES `clasificacion` (`id`),
  CONSTRAINT `eventoadv_ibfk_2` FOREIGN KEY (`idreportante`) REFERENCES `tiporeportante` (`id`),
  CONSTRAINT `eventoadv_ibfk_3` FOREIGN KEY (`idlugaro`) REFERENCES `lugaro` (`id`),
  CONSTRAINT `eventoadv_ibfk_4` FOREIGN KEY (`idtipoevento`) REFERENCES `tipoevento` (`id`),
  CONSTRAINT `eventoadv_ibfk_5` FOREIGN KEY (`idincidente`) REFERENCES `incidente` (`id`),
  CONSTRAINT `eventoadv_ibfk_6` FOREIGN KEY (`idtipoeventoadv`) REFERENCES `tipoeventoadv` (`id`),
  CONSTRAINT `eventoadv_ibfk_7` FOREIGN KEY (`idtransferido`) REFERENCES `Transferido` (`id`),
  CONSTRAINT `eventoadv_ibfk_8` FOREIGN KEY (`idfactoresmedioambiente`) REFERENCES `factoresmedioambiente` (`id`),
  CONSTRAINT `eventoadv_ibfk_9` FOREIGN KEY (`idfactoresadmon`) REFERENCES `factorestramitesadmon` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventoadv`
--

LOCK TABLES `eventoadv` WRITE;
/*!40000 ALTER TABLE `eventoadv` DISABLE KEYS */;
INSERT INTO `eventoadv` VALUES (9,1,1,'2015-05-14 04:01:17','2015-05-12','11:00:00',26,15,50,5,0,0,0,0,0,0,0,0,0,2,'descripcion modificada',0,21),(10,2,10,'2015-05-14 04:18:21','2015-05-12','11:30:00',26,15,70,2,19,56,0,8,5,10,17,10,3,2,'descripvion mod',3,22),(11,3,12,'2015-05-14 04:46:46','2015-05-05','12:00:00',1,3,70,2,14,0,0,4,5,0,5,5,3,2,'Descripcion modificadop',2,23);
/*!40000 ALTER TABLE `eventoadv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventoadvtmp`
--

DROP TABLE IF EXISTS `eventoadvtmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventoadvtmp` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `historia` bigint(15) NOT NULL,
  `edad` int(8) NOT NULL,
  `fechareporte` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fechaocurrencia` date NOT NULL,
  `hora` time NOT NULL,
  `iddependencia` int(8) NOT NULL,
  `idreportante` int(8) NOT NULL,
  `idlugaro` int(8) NOT NULL,
  `idtipoevento` int(8) NOT NULL,
  `idincidente` int(8) NOT NULL,
  `idtipoeventoadv` int(8) NOT NULL,
  `idtransferido` int(8) NOT NULL,
  `idfactoresmedioambiente` int(8) NOT NULL,
  `idfactoresadmon` int(8) NOT NULL,
  `idfactoresrh` int(8) NOT NULL,
  `idfactorestec` int(8) NOT NULL,
  `idfactortarea` int(8) NOT NULL,
  `idfactorespaciente` int(8) NOT NULL,
  `gestionado` int(8) NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL,
  `idclasificacion` int(8) NOT NULL,
  `estadot` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `edad` (`edad`),
  KEY `iddependencia` (`iddependencia`,`idreportante`,`idlugaro`),
  KEY `idincidente` (`idincidente`,`idtipoeventoadv`,`idtransferido`,`idfactoresmedioambiente`,`idfactoresadmon`),
  KEY `idfactoresrh` (`idfactoresrh`,`idfactorestec`,`idfactortarea`),
  KEY `idfactorespaciente` (`idfactorespaciente`),
  KEY `idclasificacion` (`idclasificacion`),
  KEY `idreportante` (`idreportante`),
  KEY `idlugaro` (`idlugaro`),
  KEY `idtipoevento` (`idtipoevento`),
  KEY `idtipoeventoadv` (`idtipoeventoadv`),
  KEY `idtransferido` (`idtransferido`),
  KEY `idfactoresmedioambiente` (`idfactoresmedioambiente`),
  KEY `idfactoresadmon` (`idfactoresadmon`),
  KEY `idfactorestec` (`idfactorestec`),
  KEY `idfactortarea` (`idfactortarea`),
  KEY `gestionado` (`gestionado`),
  CONSTRAINT `eventoadvtmp_ibfk_1` FOREIGN KEY (`iddependencia`) REFERENCES `dependencia` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_10` FOREIGN KEY (`idfactoresrh`) REFERENCES `factoresrh` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_11` FOREIGN KEY (`idfactorestec`) REFERENCES `factorestec` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_12` FOREIGN KEY (`idfactortarea`) REFERENCES `factorestarea` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_13` FOREIGN KEY (`idfactorespaciente`) REFERENCES `factorespaciente` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_14` FOREIGN KEY (`gestionado`) REFERENCES `gestionado` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_15` FOREIGN KEY (`idclasificacion`) REFERENCES `clasificacion` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_2` FOREIGN KEY (`idreportante`) REFERENCES `tiporeportante` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_3` FOREIGN KEY (`idlugaro`) REFERENCES `lugaro` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_4` FOREIGN KEY (`idtipoevento`) REFERENCES `tipoevento` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_5` FOREIGN KEY (`idincidente`) REFERENCES `incidente` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_6` FOREIGN KEY (`idtipoeventoadv`) REFERENCES `tipoeventoadv` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_7` FOREIGN KEY (`idtransferido`) REFERENCES `Transferido` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_8` FOREIGN KEY (`idfactoresmedioambiente`) REFERENCES `factoresmedioambiente` (`id`),
  CONSTRAINT `eventoadvtmp_ibfk_9` FOREIGN KEY (`idfactoresadmon`) REFERENCES `factorestramitesadmon` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventoadvtmp`
--

LOCK TABLES `eventoadvtmp` WRITE;
/*!40000 ALTER TABLE `eventoadvtmp` DISABLE KEYS */;
INSERT INTO `eventoadvtmp` VALUES (21,1,1,'2015-05-14 04:03:59','2015-05-12','11:00:00',26,13,70,5,0,0,0,0,0,0,0,0,0,2,'descripcion',0,1),(22,2,10,'2015-05-14 04:18:35','2015-05-12','11:30:00',26,15,70,2,19,56,0,8,5,10,17,10,3,2,'descripvion',3,1),(23,3,12,'2015-05-14 04:47:37','2015-05-05','12:00:00',1,3,70,3,0,0,0,0,0,0,0,0,0,2,'Descripcion',0,1);
/*!40000 ALTER TABLE `eventoadvtmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factoresmedioambiente`
--

DROP TABLE IF EXISTS `factoresmedioambiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factoresmedioambiente` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `factoresmedioambiente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factoresmedioambiente`
--

LOCK TABLES `factoresmedioambiente` WRITE;
/*!40000 ALTER TABLE `factoresmedioambiente` DISABLE KEYS */;
INSERT INTO `factoresmedioambiente` VALUES (0,'Ninguno'),(1,'Camilla sin barandas o fallas en las barandas'),(2,'Congestion del servicio'),(3,'Condiciones de pisos, pasamanos o planta fisica.'),(4,'Condiciones de Temperatura del ambiente, iluminaci'),(5,'Disponibilidad de Camas para la Atencion'),(6,'Disponibilidad de Quirofano'),(7,'Disponibilidad de areas de aislamiento'),(8,'Otros Medio ambiente');
/*!40000 ALTER TABLE `factoresmedioambiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factorespaciente`
--

DROP TABLE IF EXISTS `factorespaciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factorespaciente` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `factorespaciente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factorespaciente`
--

LOCK TABLES `factorespaciente` WRITE;
/*!40000 ALTER TABLE `factorespaciente` DISABLE KEYS */;
INSERT INTO `factorespaciente` VALUES (0,'Ninguno'),(1,'Paciente rechaza manejo o no colabora'),(2,'Complejidad del caso clinico'),(3,'Otros Paciente');
/*!40000 ALTER TABLE `factorespaciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factoresrh`
--

DROP TABLE IF EXISTS `factoresrh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factoresrh` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `factoresrh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factoresrh`
--

LOCK TABLES `factoresrh` WRITE;
/*!40000 ALTER TABLE `factoresrh` DISABLE KEYS */;
INSERT INTO `factoresrh` VALUES (0,'Ninguno'),(1,'Insuficiencia o no disponibilidad de recurso humano'),(3,'Falta de Competencia y entrenamiento'),(4,'Conductas o procedimientos por personal en formaci'),(5,'Falta de Soporte de otros profesionales para el manejo y toma de decisiones'),(9,'Fallas en la comunicación en el equipo de trabajo'),(10,'Otros Recurso humano');
/*!40000 ALTER TABLE `factoresrh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factorestarea`
--

DROP TABLE IF EXISTS `factorestarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factorestarea` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `factortarea` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factorestarea`
--

LOCK TABLES `factorestarea` WRITE;
/*!40000 ALTER TABLE `factorestarea` DISABLE KEYS */;
INSERT INTO `factorestarea` VALUES (0,'Ninguno'),(1,'Fallas en Informacion al paciente y la famillia'),(2,'Fallas en los procesos de esterilizacion'),(3,'No Adherencia a guías, procedimientos y otros.'),(4,'Ausencia de  guías, procedimientos y otros.'),(5,'Falla Calidad del Registro Clinico (forma y contenido)'),(6,'Error en la identificacion o marcacion de estudio imagenologico'),(7,'Transfusion: Equivocacion en paciente, componente , cantidad'),(8,'Error en  informe  de laboratorio clinico.'),(9,'Error en la marcacion de  muestras  de laboratorio'),(10,'Otros Area');
/*!40000 ALTER TABLE `factorestarea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factorestec`
--

DROP TABLE IF EXISTS `factorestec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factorestec` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `factorestec` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factorestec`
--

LOCK TABLES `factorestec` WRITE;
/*!40000 ALTER TABLE `factorestec` DISABLE KEYS */;
INSERT INTO `factorestec` VALUES (0,'Ninguno'),(1,'Falta Dotacion para la atencion'),(2,'No disponibilidad de Tecnologia (imágenes, laboratorio y otros)'),(4,'No Oportunidad en  Transporte asistencial '),(5,'Falta Mantenimiento de equipos '),(7,'No Disponibilidad de Insumos o reactivos'),(8,'No Disponibilidad de Material Medico Quirugico'),(9,'Fallas en Calidad de  los dispositivos biomedicos o insumos'),(10,'No Disponibilidad de Medicamentos'),(11,'No Disponibilidad  de Quimioterapia'),(12,'No Disponibilidad  de Radioterapia'),(13,'No Disponibilidad de material esteril (Ropa, equipos y otros de central)'),(14,'No Disponibilidad de Quirofano'),(15,'No Disponibilidad  de UCI'),(17,'Otros Tecnologia ');
/*!40000 ALTER TABLE `factorestec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factorestramitesadmon`
--

DROP TABLE IF EXISTS `factorestramitesadmon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factorestramitesadmon` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `factoresadmon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factorestramitesadmon`
--

LOCK TABLES `factorestramitesadmon` WRITE;
/*!40000 ALTER TABLE `factorestramitesadmon` DISABLE KEYS */;
INSERT INTO `factorestramitesadmon` VALUES (0,'Ninguno'),(1,'Falla en Autorizaciones aseguradoras  o entes territoriales'),(3,'Retrasos en la respuesta de Centros de Referencia'),(4,'Fallas en Soporte administrativo o gerencial'),(5,'Otros  Retrasos Administrativos');
/*!40000 ALTER TABLE `factorestramitesadmon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gestionado`
--

DROP TABLE IF EXISTS `gestionado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gestionado` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `gestionado` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gestionado`
--

LOCK TABLES `gestionado` WRITE;
/*!40000 ALTER TABLE `gestionado` DISABLE KEYS */;
INSERT INTO `gestionado` VALUES (1,'SI'),(2,'NO');
/*!40000 ALTER TABLE `gestionado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidente`
--

DROP TABLE IF EXISTS `incidente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidente` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `incidente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clase` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidente`
--

LOCK TABLES `incidente` WRITE;
/*!40000 ALTER TABLE `incidente` DISABLE KEYS */;
INSERT INTO `incidente` VALUES (0,'Seleccione',0),(9,'CAIDA INTRA INSTITUCIONAL',0),(10,'RETIRO ACCIDENTAL DE DISPOSITIVO ',0),(11,'INTENTO DE SUICIDIO INTRA INSTITUCIONAL',0),(12,'INCIDENTE ASOCIADO A RADIOTERAPIA',0),(13,'INCIDENTE  POR ERROR EN INFORME DE IMÁGENES DIAGNO',0),(14,'INCIDENTE POR ERROR EN INFORME DE LABORATORIO CLIN',0),(15,'INCIDENTE POR ERROR EN INFORME DE PATOLOGIA',0),(16,'INCIDENTE POR FALLAS EN ESTERILIZACION DE EQUIPOS',0),(17,'PERDIDA O FUGA DE PACIENTE PSIQUIATRICO',0),(18,'PERDIDA O FUGA DE PACIENTE MENOR',0),(19,'OTRO INCIDENTE ',0);
/*!40000 ALTER TABLE `incidente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lugaro`
--

DROP TABLE IF EXISTS `lugaro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lugaro` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `lugar0` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clase` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lugaro`
--

LOCK TABLES `lugaro` WRITE;
/*!40000 ALTER TABLE `lugaro` DISABLE KEYS */;
INSERT INTO `lugaro` VALUES (1,'HOSPITALIZACION',100),(2,'Medica Hombres',111),(3,'Medica Mujeres',112),(4,'Cirugia Hombres',113),(5,'Cirugia Mujeres',114),(6,'Ortopedia',115),(7,'Infecto adultos',116),(8,'Neurocirugia',117),(9,'Pediatria general',118),(10,'Ana Frank',119),(11,'Anhelo de vida',120),(12,'Unidad de Salud Mental',121),(13,'Cirena',122),(14,'Cipaf',123),(15,'Ucin',124),(16,'UCI 2',125),(17,'UCI 3',126),(18,'UCI 4',127),(19,'UCI Neurocirugia',128),(20,'UCI Urgencias',129),(21,'Unidad de Quemados',130),(22,'Ginecologia',131),(23,'Embarazo alto riesgo obstetrico',132),(24,'Puerperio',133),(25,'Hospitalizacion domiciliaria',134),(26,'Sala de Partos',135),(27,'Hemato Oncologia',136),(30,'URGENCIAS Y TRANSPORTE',200),(31,'Admision partos',201),(32,'Unidad de trauma',202),(33,'Pediatria urgencias y trauma pediatrico',203),(34,'Consultorios',204),(35,'Observacion cirugia',205),(36,'Sala de yesos',206),(37,'Medicina Interna urgencias',207),(38,'Transporte asistencial',208),(40,'CONSULTA EXTERNA Y TERAPIAS',300),(41,'Consultorios Especialidades',301),(44,'Radioterapia',304),(45,'Quimioterapia',305),(46,'Medicina Fisica y Rehabilitacion',306),(47,'Odontologia',307),(50,'AYUDAS DIAGNOSTICAS',400),(51,'Radiologia',401),(52,'Unidad de dialisis Renal -RTS',402),(53,'Unidad de endoscopia digestiva',403),(54,'Unidad de Diagnostico Cardiovascular ',404),(55,'Laboratorio clinico y Patologia',405),(56,'Banco de Sangre',406),(57,'Medicina nuclear',407),(58,'Angiografía y Hemodinamia',408),(60,'QUIROFANOS',500),(61,'Quirofano central',501),(62,'Quirofano neurocirugia',502),(63,'Quirofano oftalmologia',503),(64,'Quirofano otorrinolaringologia',504),(65,'Quirofano urgencias',505),(66,'Quirofano ginecologia y obstetricia',506),(67,'Quirofano quemados',507),(70,'OTRAS Areas Del HUV',600);
/*!40000 ALTER TABLE `lugaro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otros`
--

DROP TABLE IF EXISTS `otros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_eventoadv` int(11) NOT NULL,
  `dependencia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reportante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lugar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Incidente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tipoevento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `transferido` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facambiente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facadmon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facrh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `factec` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facarea` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facpac` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_eventoadv` (`id_eventoadv`),
  CONSTRAINT `otros_ibfk_1` FOREIGN KEY (`id_eventoadv`) REFERENCES `eventoadv` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otros`
--

LOCK TABLES `otros` WRITE;
/*!40000 ALTER TABLE `otros` DISABLE KEYS */;
INSERT INTO `otros` VALUES (1,9,'Otrica dependencia','esta la cambie','','','','','','','','','',''),(2,10,'otra dependencia','Otro reportante','Otro lugar','Otro Incidente','Otro tipo de eveto adverso','','Otro factor medio ambiente','Otro factor administrativo','Otro recurso humano','Otro factor tecnologico','Otro factor tara','Otro factor paciente'),(3,11,'','','Por mi casita','','','','','Pecuecas','','','','NO quiso dejarse aplicar');
/*!40000 ALTER TABLE `otros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otrostmp`
--

DROP TABLE IF EXISTS `otrostmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otrostmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_eventoadv` int(11) NOT NULL,
  `dependencia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reportante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lugar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Incidente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tipoevento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `transferido` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facambiente` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facadmon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facrh` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `factec` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facarea` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facpac` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_eventoadv` (`id_eventoadv`),
  CONSTRAINT `otrostmp_ibfk_1` FOREIGN KEY (`id_eventoadv`) REFERENCES `eventoadvtmp` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otrostmp`
--

LOCK TABLES `otrostmp` WRITE;
/*!40000 ALTER TABLE `otrostmp` DISABLE KEYS */;
INSERT INTO `otrostmp` VALUES (1,21,'Otrica dependencia','','Otra area','','','','','','','','',''),(2,22,'otra dependencia','Otro reportante','Otro lugar','Otro Incidente','Otro tipo de eveto adverso','','Otro factor medio ambiente','Otro factor administrativo','Otro recurso humano','Otro factor tecnologico','Otro factor tara','Otro factor paciente'),(3,23,'','','Por mi casita','','','','','','','','','');
/*!40000 ALTER TABLE `otrostmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoevento`
--

DROP TABLE IF EXISTS `tipoevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoevento` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tipoevento` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoevento`
--

LOCK TABLES `tipoevento` WRITE;
/*!40000 ALTER TABLE `tipoevento` DISABLE KEYS */;
INSERT INTO `tipoevento` VALUES (1,'INCIDENTE'),(2,'EVENTO ADVERSO PREVENIBLE'),(3,'EVENTO ADVERSO NO PREVENIBLE'),(4,'COMPLICACION'),(5,'SIN CLASIFICAR');
/*!40000 ALTER TABLE `tipoevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoeventoadv`
--

DROP TABLE IF EXISTS `tipoeventoadv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoeventoadv` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `tipoeventoadv` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `clase` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoeventoadv`
--

LOCK TABLES `tipoeventoadv` WRITE;
/*!40000 ALTER TABLE `tipoeventoadv` DISABLE KEYS */;
INSERT INTO `tipoeventoadv` VALUES (0,'Seleccione',0),(9,'REACCION ADVERSA TRANSFUSIONAL ',200),(10,'ULCERA POR PRESION',300),(11,'CAIDA INTRA INSTITUCIONAL',300),(12,'FLEBITIS POR VENOPUNCION',400),(13,'BRONCO ASPIRACION DURANTE LA ATENCION',500),(14,'LUXACION POST QUIRUGICO REMPLAZO CADERA',600),(15,'RETIRO ACCIDENTAL DE DISPOSITIVO ',700),(16,'NEUMOTORAX ASOCIADO A LA ATENCION',800),(17,'TROMBOEMBOLISMO DURANTE LA ATENCION',900),(18,'INFARTO AGUDO MIOCARDIO  DURANTE LA INTERNACION',1000),(19,'ECLAMPSIA INTRAHOSPITALARIA',1100),(20,'ASFIXIA PERINATAL',1200),(21,'LESION A NEONATO EN PARTO O CESAREA',1300),(22,'HISTERECTOMIA POST EVENTO OBSTETRICO',1400),(23,'CHOQUE HIPOVOLEMICO-EVENTO OBSTETRICO',1500),(24,'DEPRESIÓN RESPIRATORIA POST QX INMEDIATO',1600),(25,'REINTERVENCIÓN QUIRÚRGICA POR SANGRADO',1700),(26,'CUERPO EXTRAÑO OLVIDADO EN PROCEDIMIENTO QUIRUGICO',1800),(27,'CIRUGIA EN PARTE EQUIVOCADA',1900),(28,'CIRUGIA O PROCEDIMIENTO EN PACIENTE EQUIVOCADO',2000),(30,'PROCEDIMIENTO QUIRUGICO NO PERTINENTE',2200),(31,'LESION DE ORGANO DURANTE PROCEDIMIENTO QUIRUGICO',2300),(32,'EVENTRACION O EVICERACION POST QUIRUGICA',2400),(33,'DEHICENCIA DE SUTURA',2500),(34,'QUEMADURA DURANTE LA ATENCION',2600),(37,'INTENTO DE SUICIDIO INTRA INSTITUCIONAL',2700),(38,'SUICIDIO INTRA INSTITUCIONAL',2800),(39,'EVENTO ASOCIADO A RADIOTERAPIA',2900),(40,'DETERIORO CLINICO POR RETRASOS EN LA ATENCION ',3000),(41,'Retraso Quirurgico',3001),(42,'Retraso en Imágenes Diagnosticas',3002),(43,'Retraso en examen Laboratorio Clinico',3003),(44,'Retraso en disponibilidad de medicamanto',3004),(45,'Retraso en Hemocomponentes',3005),(46,'Retraso valoracion y definicion de conducta',3006),(47,'Error o Retraso en el Diagnostico clinico',3007),(48,'Disponibilidad de UCI',3008),(51,'EVENTO ADVERSO POR ERROR EN INFORME DE IMÁGENES DI',3100),(52,'EVENTO ADVERSO POR ERROR EN INFORME DE LABORATORIO',3200),(53,'EVENTO ADVERSO POR ERROR EN INFORME DE PATOLOGIA',3300),(54,'EVENTO ADVERSO POR FALLAS EN ESTERILIZACION DE EQU',3400),(55,'INFECCION ASOCIADA AL CUIDADO',3500),(56,'OTRO EVENTO ADVERSO',3600);
/*!40000 ALTER TABLE `tipoeventoadv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiporeportante`
--

DROP TABLE IF EXISTS `tiporeportante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiporeportante` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `reportante` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiporeportante`
--

LOCK TABLES `tiporeportante` WRITE;
/*!40000 ALTER TABLE `tiporeportante` DISABLE KEYS */;
INSERT INTO `tiporeportante` VALUES (1,'Medico'),(2,'Enfermero(a) profesional'),(3,'Auxiliar enfermeria/Tecnologo'),(4,'Bacteriologo'),(5,'Nutricionista'),(6,'Odontólogo'),(7,'QF o Personal de Farmacia'),(8,'Residente'),(9,'Estudiante de pregrado'),(10,'Personal Administrativo'),(11,'Reporte por ente externo'),(13,'Terapeuta/Fisioterapeuta'),(15,'Otro');
/*!40000 ALTER TABLE `tiporeportante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_usr` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'mich','adminhuv',''),(3,'losorio','9708',''),(4,'msanchez','5678',''),(5,'bvilla','paquito',''),(6,'tramos','urgencia',''),(7,'lpino','cete',''),(8,'lpalacios','MARZO',''),(9,'csanchez','imadx',''),(10,'aapraez','uci',''),(11,'operez','uti',''),(12,'medicas','4790',''),(13,'ggaleano','sogg',''),(14,'agonzalez','adogonza',''),(15,'mhincapie','hincacx',''),(16,'azorrilla','azmn',''),(17,'jrojas','mnjcr',''),(18,'gvargas','gvrn',''),(19,'jtorres','jtped',''),(20,'rmina','pediatrico',''),(21,'jroa','jaime',''),(22,'eacevedo','3110',''),(23,'jfonseca','mme',''),(24,'mlopez','crislopez',''),(25,'ldelgado','ortopedia',''),(26,'hvalencia','191970',''),(27,'dmondragon','dollymon',''),(28,'oarboleda','hemataonco',''),(29,'olenis','olga40',''),(30,'eguevara','transplant',''),(31,'pthorpt','pilar',''),(32,'agonzalez','transfusio',''),(33,'acortes','hemocentro',''),(34,'hgomez','24618328',''),(35,'sossa','sossave',''),(36,'cpallares','cpve',''),(37,'farmatecno','nr8812',''),(38,'amich','adminhuv','admin'),(40,'damir','4d186321c1a7f0f354b297e8914ab240',''),(41,'admin','21232f297a57a5a743894a0e4a801fc3','');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-14  0:01:05
