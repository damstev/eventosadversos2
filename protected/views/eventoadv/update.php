<?php
$this->breadcrumbs=array(
	'Eventoadvs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Eventoadv','url'=>array('index')),
	array('label'=>'Create Eventoadv','url'=>array('create')),
	array('label'=>'View Eventoadv','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Eventoadv','url'=>array('admin')),
	);
	?>

	<h1>Update Eventoadv <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>