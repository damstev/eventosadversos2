<?php 

$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/eventosadv.js');

$model->idlugaro = '2';

$form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'eventoadvtmp-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'htmlOptions' => array('class' => 'well'), // for inset effect
)); ?>

<!-- <p class="help-block">Fields with <span class="required">*</span> are required.</p> -->

<?php echo $form->errorSummary($model); ?>
<?php echo $form->errorSummary($modelOtros); ?>

	<?php echo $form->textFieldGroup($model,'historia',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'col-sm-5','maxlength'=>15)))); ?>

	<?php echo $form->dropDownListGroup($model, 'edad', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Edad::model()->findAll(), 'id', 'edad')
			)
	)); ?>

	<?php echo $form->datePickerGroup($model,'fechaocurrencia',array('widgetOptions'=>array('options'=>array('language' => 'es', 'format' => 'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>', 'append'=>'Click en Mes/Año para seleccionar un Mes/Año diferente.')); ?>

	<?php echo $form->timePickerGroup($model,'hora',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>


	<?php echo $form->dropDownListGroup($model, 'iddependencia', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Dependencia::model()->findAll(), 'id', 'dependencia'),
				'htmlOptions' => array('class' => 'trigger_otros')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'dependencia',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_iddependencia', 'placeholder' => 'Otra Dependencia')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idreportante', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Tiporeportante::model()->findAll(), 'id', 'reportante'),
				'htmlOptions' => array('class' => 'trigger_otros')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'reportante',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idreportante', 'placeholder' => 'Otro Reportante')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idlugaro', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Lugaro::model()->findAll(), 'id', 'lugar0'),
				'htmlOptions' => array('class' => 'trigger_otros')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'lugar',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idlugaro', 'placeholder' => 'Otro Lugar')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idtipoevento', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Tipoevento::model()->findAll(), 'id', 'tipoevento'),				
			)
	)); ?>


	<?php echo $form->dropDownListGroup($model, 'idclasificacion', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Clasificacion::model()->findAll(), 'id', 'clasificacion'),
				'htmlOptions' => array('class' => 'campo_prevenible combo_dependiente')
			)
	)); ?>


	<?php echo $form->dropDownListGroup($model, 'idincidente', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Incidente::model()->findAll(), 'id', 'incidente'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'Incidente',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idincidente', 'placeholder' => 'Otro Incidente')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idtipoeventoadv', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Tipoeventoadv::model()->findAll(), 'id', 'tipoeventoadv'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'tipoevento',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idtipoeventoadv', 'placeholder' => 'Otro Tipo Evento')
	))); ?>


	<?php /*echo $form->dropDownListGroup($model, 'idtransferido', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Transferido::model()->findAll(), 'id', 'transferido')
			)
	));*/ ?>
	<?php //echo $form->textFieldGroup($model,'idtransferido',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>


	<?php echo $form->dropDownListGroup($model, 'idfactoresmedioambiente', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Factoresmedioambiente::model()->findAll(), 'id', 'factoresmedioambiente'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'facambiente',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idfactoresmedioambiente', 'placeholder' => 'Otro Factor')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idfactoresadmon', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Factorestramitesadmon::model()->findAll(), 'id', 'factoresadmon'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'facadmon',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idfactoresadmon', 'placeholder' => 'Otro Factor')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idfactoresrh', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Factoresrh::model()->findAll(), 'id', 'factoresrh'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'facrh',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idfactoresrh', 'placeholder' => 'Otro Factor')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idfactorestec', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Factorestec::model()->findAll(), 'id', 'factorestec'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'factec',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idfactorestec', 'placeholder' => 'Otro Factor')
	))); ?>

	<?php echo $form->dropDownListGroup($model, 'idfactortarea', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Factorestarea::model()->findAll(), 'id', 'factortarea'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'facarea',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idfactortarea', 'placeholder' => 'Otro Factor')
	))); ?>


	<?php echo $form->dropDownListGroup($model, 'idfactorespaciente', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Factorespaciente::model()->findAll(), 'id', 'factorespaciente'),
				'htmlOptions' => array('class' => 'campo_prevenible trigger_otros combo_dependiente')
			)
	)); ?>
	<?php echo $form->textFieldGroup($modelOtros,'facpac',array('label'=>'', 'widgetOptions'=>array(
		'htmlOptions'=>array('class'=>'span5', 'id' => 'otros_idfactorespaciente', 'placeholder' => 'Otro Factor')
	))); ?>

	<?php /*echo $form->dropDownListGroup($model, 'gestionado', array(
		'widgetOptions' => array(
				'data' => CHtml::listData(Gestionado::model()->findAll(), 'id', 'gestionado'),
				'htmlOptions' => array('class' => 'campo_prevenible')
			)
	)); */ ?>
	<?php //echo $form->textFieldGroup($model,'gestionado',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textAreaGroup($model,'observaciones', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	
	<?php //echo $form->textFieldGroup($model,'idclasificacion',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php //echo $form->textFieldGroup($model,'estadot',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=> 'Enviar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
