<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('historia')); ?>:</b>
	<?php echo CHtml::encode($data->historia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('edad')); ?>:</b>
	<?php echo CHtml::encode($data->edad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechareporte')); ?>:</b>
	<?php echo CHtml::encode($data->fechareporte); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaocurrencia')); ?>:</b>
	<?php echo CHtml::encode($data->fechaocurrencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hora')); ?>:</b>
	<?php echo CHtml::encode($data->hora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iddependencia')); ?>:</b>
	<?php echo CHtml::encode($data->iddependencia); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('idreportante')); ?>:</b>
	<?php echo CHtml::encode($data->idreportante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idlugaro')); ?>:</b>
	<?php echo CHtml::encode($data->idlugaro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idtipoevento')); ?>:</b>
	<?php echo CHtml::encode($data->idtipoevento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idincidente')); ?>:</b>
	<?php echo CHtml::encode($data->idincidente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idtipoeventoadv')); ?>:</b>
	<?php echo CHtml::encode($data->idtipoeventoadv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idtransferido')); ?>:</b>
	<?php echo CHtml::encode($data->idtransferido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idfactoresmedioambiente')); ?>:</b>
	<?php echo CHtml::encode($data->idfactoresmedioambiente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idfactoresadmon')); ?>:</b>
	<?php echo CHtml::encode($data->idfactoresadmon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idfactoresrh')); ?>:</b>
	<?php echo CHtml::encode($data->idfactoresrh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idfactorestec')); ?>:</b>
	<?php echo CHtml::encode($data->idfactorestec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idfactortarea')); ?>:</b>
	<?php echo CHtml::encode($data->idfactortarea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idfactorespaciente')); ?>:</b>
	<?php echo CHtml::encode($data->idfactorespaciente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gestionado')); ?>:</b>
	<?php echo CHtml::encode($data->gestionado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idclasificacion')); ?>:</b>
	<?php echo CHtml::encode($data->idclasificacion); ?>
	<br />

	*/ ?>

</div>