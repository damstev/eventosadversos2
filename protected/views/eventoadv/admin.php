<h2>Listado de eventos validados</h2>
<?php $this->widget('booster.widgets.TbExtendedGridView',array(
'id'=>'eventoadvtmp-grid',
'dataProvider'=>$model->search2(),
'filter'=>$model,
'responsiveTable' => true,
'columns'=>array(
		array('name' => 'id', 'header' => 'Id Evento Adverso'),
		array('name' => 'historia', 'header' => 'Numero de Cédula'),
		array('name' => 'dependencia_search', 'value'=>'$data->iddependencia0 ? $data->iddependencia0->dependencia : ""', 'header' => 'Dependencia'),
		array('name' => 'fechareporte', 'header' => 'Fecha Reporte'),
		array('name' => 'fechaocurrencia', 'header' => 'Fecha Ocurrencia'),
		array(
			'class'=>'booster.widgets.TbButtonColumn',
			'header' => 'Ver',
			'template'=>'{view}',
			'buttons'=>array(	
				'view' => array
                (
                    'url'=>'Yii::app()->createUrl("eventoadv/view", array("id"=>$data->id))',
                    'options'=>array(
                        'class'=>'btn btn-sm btn-info',
                        'title'=>'Ver',
                    ),
                ),
			)
		),
	),
)); ?>
