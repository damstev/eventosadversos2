<?php 

### BEGIN CONFIGURACION EVENTO ACTUAL ###
$attributes = array(
		'id',
		'historia',
		'edad0.edad',
		'fechareporte',
		'fechaocurrencia',
		'hora',
		//'iddependencia0.dependencia',
		array('name' => 'Dependencia', 'value' =>  ($model->iddependencia==='26') ? $modelOtros->dependencia : $model->iddependencia0->dependencia),
		//'idreportante0.reportante',
		array('name' => 'Reportante', 'value' =>  ($model->idreportante==='15') ? $modelOtros->reportante : $model->idreportante0->reportante),
		//'idlugaro0.lugar0',
		array('name' => 'Lugar', 'value' =>  ($model->idlugaro==='70') ? $modelOtros->lugar : $model->idlugaro0->lugar0),
		'idtipoevento0.tipoevento',						
		//'gestionado',				
	);
if($model->idtipoevento === "1"){
	$attributes = array_merge($attributes,array(
		//'idincidente0.incidente',
		array('name' => 'Incidente', 'value' =>  ($model->idincidente==='19') ? $modelOtros->Incidente : $model->idincidente0->incidente),
		//'idtransferido0.transferido',		
		//'idfactoresmedioambiente0.factoresmedioambiente',
		array('name' => 'Factores Medio Ambiente', 'value' =>  ($model->idfactoresmedioambiente==='8') ? $modelOtros->facambiente : $model->idfactoresmedioambiente0->factoresmedioambiente),
		//'idfactoresadmon0.factoresadmon',
		array('name' => 'Factores Administrativos', 'value' =>  ($model->idfactoresadmon==='5') ? $modelOtros->facadmon : $model->idfactoresadmon0->factoresadmon),
		//'idfactoresrh0.factoresrh',
		array('name' => 'Factores de Recurso Humano', 'value' =>  ($model->idfactoresrh==='10') ? $modelOtros->facrh : $model->idfactoresrh0->factoresrh),
		//'idfactorestec0.factorestec',
		array('name' => 'Factores Tecnología', 'value' =>  ($model->idfactorestec==='17') ? $modelOtros->factec : $model->idfactorestec0->factorestec),
		//'idfactortarea0.factortarea',
		array('name' => 'Factores Area', 'value' =>  ($model->idfactortarea==='10') ? $modelOtros->facarea : $model->idfactortarea0->factortarea),
		//'idfactorespaciente0.factorespaciente',
		array('name' => 'Factores del Paciente', 'value' =>  ($model->idfactorespaciente==='3') ? $modelOtros->facpac : $model->idfactorespaciente0->factorespaciente),
	));
}
elseif($model->idtipoevento === "2"){
	$attributes = array_merge($attributes,array(
		'idclasificacion0.clasificacion',
		//'idtipoeventoadv0.tipoeventoadv',
		array('name' => 'Tipo de Evento Adverso', 'value' =>  ($model->idtipoeventoadv==='56') ? $modelOtros->tipoevento : $model->idtipoeventoadv0->tipoeventoadv),
		//'idtransferido0.transferido',		
		//'idfactoresmedioambiente0.factoresmedioambiente',
		array('name' => 'Factores Medio Ambiente', 'value' =>  ($model->idfactoresmedioambiente==='8') ? $modelOtros->facambiente : $model->idfactoresmedioambiente0->factoresmedioambiente),
		//'idfactoresadmon0.factoresadmon',
		array('name' => 'Factores Administrativos', 'value' =>  ($model->idfactoresadmon==='5') ? $modelOtros->facadmon : $model->idfactoresadmon0->factoresadmon),
		//'idfactoresrh0.factoresrh',
		array('name' => 'Factores de Recurso Humano', 'value' =>  ($model->idfactoresrh==='10') ? $modelOtros->facrh : $model->idfactoresrh0->factoresrh),
		//'idfactorestec0.factorestec',
		array('name' => 'Factores Tecnología', 'value' =>  ($model->idfactorestec==='17') ? $modelOtros->factec : $model->idfactorestec0->factorestec),
		//'idfactortarea0.factortarea',
		array('name' => 'Factores Area', 'value' =>  ($model->idfactortarea==='10') ? $modelOtros->facarea : $model->idfactortarea0->factortarea),
		//'idfactorespaciente0.factorespaciente',
		array('name' => 'Factores del Paciente', 'value' =>  ($model->idfactorespaciente==='3') ? $modelOtros->facpac : $model->idfactorespaciente0->factorespaciente),
	));
}
$attributes[] = 'observaciones';
### END CONFIGURACION EVENTO ACTUAL ###


### BEGIN CONFIGURACION EVENTO ORIGNAL ###
$attributesTmp = array(
		'id',
		'historia',
		'edad0.edad',
		'fechareporte',
		'fechaocurrencia',
		'hora',
		//'iddependencia0.dependencia',
		array('name' => 'Dependencia', 'value' =>  ($modelTmp->iddependencia==='26') ? $modelOtrosTmp->dependencia : $model->iddependencia0->dependencia),
		//'idreportante0.reportante',
		array('name' => 'Reportante', 'value' =>  ($modelTmp->idreportante==='15') ? $modelOtrosTmp->reportante : $model->idreportante0->reportante),
		//'idlugaro0.lugar0',
		array('name' => 'Lugar', 'value' =>  ($modelTmp->idlugaro==='70') ? $modelOtrosTmp->lugar : $model->idlugaro0->lugar0),
		'idtipoevento0.tipoevento',				
		//'gestionado',				
	);
if($modelTmp->idtipoevento === "1"){
	$attributesTmp = array_merge($attributesTmp,array(
		//'idincidente0.incidente',
		array('name' => 'Incidente', 'value' =>  ($modelTmp->idincidente==='19') ? $modelOtrosTmp->Incidente : $modelTmp->idincidente0->incidente),
		//'idtransferido0.transferido',		
		//'idfactoresmedioambiente0.factoresmedioambiente',
		array('name' => 'Factores Medio Ambiente', 'value' =>  ($modelTmp->idfactoresmedioambiente==='8') ? $modelOtrosTmp->facambiente : $modelTmp->idfactoresmedioambiente0->factoresmedioambiente),
		//'idfactoresadmon0.factoresadmon',
		array('name' => 'Factores Administrativos', 'value' =>  ($modelTmp->idfactoresadmon==='5') ? $modelOtrosTmp->facadmon : $modelTmp->idfactoresadmon0->factoresadmon),
		//'idfactoresrh0.factoresrh',
		array('name' => 'Factores de Recurso Humano', 'value' =>  ($modelTmp->idfactoresrh==='10') ? $modelOtrosTmp->facrh : $modelTmp->idfactoresrh0->factoresrh),
		//'idfactorestec0.factorestec',
		array('name' => 'Factores Tecnología', 'value' =>  ($modelTmp->idfactorestec==='17') ? $modelOtrosTmp->factec : $modelTmp->idfactorestec0->factorestec),
		//'idfactortarea0.factortarea',
		array('name' => 'Factores Area', 'value' =>  ($modelTmp->idfactortarea==='10') ? $modelOtrosTmp->facarea : $modelTmp->idfactortarea0->factortarea),
		//'idfactorespaciente0.factorespaciente',
		array('name' => 'Factores del Paciente', 'value' =>  ($modelTmp->idfactorespaciente==='3') ? $modelOtrosTmp->facpac : $modelTmp->idfactorespaciente0->factorespaciente),
	));
}
elseif($modelTmp->idtipoevento === "2"){
	$attributesTmp = array_merge($attributesTmp,array(
		'idclasificacion0.clasificacion',	
		//'idtransferido0.transferido',		
		//'idfactoresmedioambiente0.factoresmedioambiente',
		array('name' => 'Factores Medio Ambiente', 'value' =>  ($modelTmp->idfactoresmedioambiente==='8') ? $modelOtrosTmp->facambiente : $modelTmp->idfactoresmedioambiente0->factoresmedioambiente),
		//'idfactoresadmon0.factoresadmon',
		array('name' => 'Factores Administrativos', 'value' =>  ($modelTmp->idfactoresadmon==='5') ? $modelOtrosTmp->facadmon : $modelTmp->idfactoresadmon0->factoresadmon),
		//'idfactoresrh0.factoresrh',
		array('name' => 'Factores de Recurso Humano', 'value' =>  ($modelTmp->idfactoresrh==='10') ? $modelOtrosTmp->facrh : $modelTmp->idfactoresrh0->factoresrh),
		//'idfactorestec0.factorestec',
		array('name' => 'Factores Tecnología', 'value' =>  ($modelTmp->idfactorestec==='17') ? $modelOtrosTmp->factec : $modelTmp->idfactorestec0->factorestec),
		//'idfactortarea0.factortarea',
		array('name' => 'Factores Area', 'value' =>  ($modelTmp->idfactortarea==='10') ? $modelOtrosTmp->facarea : $modelTmp->idfactortarea0->factortarea),
		//'idfactorespaciente0.factorespaciente',
		array('name' => 'Factores del Paciente', 'value' =>  ($modelTmp->idfactorespaciente==='3') ? $modelOtrosTmp->facpac : $modelTmp->idfactorespaciente0->factorespaciente),
	));
}
$attributesTmp[] = 'observaciones';
### END CONFIGURACION EVENTO ORIGINAL ###





?>

<div class="row">
	<div class="col-md-6">
		<div class="alert alert-warning" role="alert">Evento Registrado originalmente</div>
	</div>
	<div class="col-md-6">
		<div class="alert alert-success" role="alert">Evento Actual</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<?php
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$modelTmp,
			'attributes'=>$attributesTmp,
		)); 
		?>
	</div>
	<div class="col-md-6">
		<?php
		$this->widget('booster.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>$attributes,
		)); 
		?>
	</div>
</div>