<?php
$this->breadcrumbs=array(
	'Eventoadvtmps'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Eventoadvtmp','url'=>array('index')),
array('label'=>'Create Eventoadvtmp','url'=>array('create')),
array('label'=>'Update Eventoadvtmp','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Eventoadvtmp','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Eventoadvtmp','url'=>array('admin')),
);
?>

<h1>View Eventoadvtmp #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'historia',
		'edad',
		'fechareporte',
		'fechaocurrencia',
		'hora',
		'iddependencia',
		'idreportante',
		'idlugaro',
		'idtipoevento',
		'idincidente',
		'idtipoeventoadv',
		'idtransferido',
		'idfactoresmedioambiente',
		'idfactoresadmon',
		'idfactoresrh',
		'idfactorestec',
		'idfactortarea',
		'idfactorespaciente',
		'gestionado',
		'observaciones',
		'idclasificacion',
		'estadot',
),
)); ?>
