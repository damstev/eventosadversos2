<?php
$this->breadcrumbs=array(
	'Eventoadvtmps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Eventoadvtmp','url'=>array('index')),
	array('label'=>'Create Eventoadvtmp','url'=>array('create')),
	array('label'=>'View Eventoadvtmp','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Eventoadvtmp','url'=>array('admin')),
	);
	?>

	<h1>Update Eventoadvtmp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>