<?php
//$this->layout = 'basic';
$this->pageTitle = 'gnio - entrar';
?>
<?php if (Yii::app()->user->hasFlash('loginflash')): ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('loginflash'); ?>
    </div>
<?php else: ?>
    <div class="form-signin">
        <?php
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'logon-form',
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>
        <h2 class="form-signin-heading">Ingrese</h2>

        <?php echo $form->textField($model, 'username', array('class' => 'input-block-level', 'placeholder' => 'Usuario')); ?>
        <?php echo $form->error($model, 'username'); ?>

        <?php echo $form->passwordField($model, 'password', array('class' => 'input-block-level', 'placeholder' => 'Contraseña')); ?>
        <?php echo $form->error($model, 'password'); ?>

        <?php //echo $form->checkbox($model, 'rememberMe'); ?>

        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'label' => 'Entrar',
            'context' => 'primary',
            'size' => 'large',
            'url' => $this->createUrl('micrugeui/login')
        ));
        
        echo '&nbsp;';
        ?>   




        <?php $this->endWidget(); ?>
    </div>
<?php endif; ?>