<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	public function authenticate()
    {
        $username=strtolower($this->username);
        $user=Usuarios::model()->find('LOWER(usuario)=?',array($username));

        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if (md5($this->password) !== $user->password) {
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->_id=$user->id;
            $this->username=$user->usuario;
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }
 
    public function authenticate2()
    {
        //$username=strtolower($this->username);
        $user=Usuarios::model()->find('usuario=? AND password=?',array($this->username, $this->password));
        if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else
        {
            $this->_id=$user->id;
            $this->username=$user->usuario;
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}