<?php

class EventoadvController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
	return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
	return array(
	array('allow', // allow authenticated user to perform 'create' and 'update' actions
		'actions'=>array('create', 'admin', 'view'),
		'users'=>array('@'),
		),
	array('deny',  // deny all users
		'users'=>array('*'),
		),
	);
}


/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
	$model = $this->loadModel($id);
	$modelOtros = $this->loadModelOtros($id);

	$modelTmp = $this->loadModelTmp($model->ideventoadvtmp);
	$modelOtrosTmp = $this->loadModelOtrosTmp($model->ideventoadvtmp);

	$this->render('view',array(
		'model'=>$model,
		'modelOtros'=>$modelOtros,
		'modelTmp'=>$modelTmp,
		'modelOtrosTmp'=>$modelOtrosTmp
		));
}

/**
* $id es la llave primaria de eventoadvtmp!
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate($id)
{
	$model=new Eventoadv;
	$modeltTmp=$this->loadModelTmp($id);
	$model->attributes = $modeltTmp->getAttributes();

	$modelOtros=new Otros;
	$modelOtrosTmp = $this->loadModelOtrosTmp($id);
	$modelOtros->attributes = $modelOtrosTmp->getAttributes();

	if(isset($_POST['Eventoadv']))
	{
		$model->attributes=$_POST['Eventoadv'];
		$model->ideventoadvtmp = $modeltTmp->id;
		$modelOtros->attributes=$_POST['Otros'];

		$transaction = Yii::app()->db->beginTransaction();
		try 
		{
			if(!$model->save())
				throw new Exception('Error al guardar el evento');

			$modelOtros->id_eventoadv = (int)$model->id;
			if(!$modelOtros->save())
				throw new Exception('Error al guardar el evento(otros)');

			/*$modeltTmp->estadot=1;
			if($modeltTmp->save())
				throw new Exception('Error al guardar el evento(cambiar estado al original)');
				*/
			$command = Yii::app()->db->createCommand();
			$command->update('eventoadvtmp', array(
			    'estadot'=>1,
			), 'id=:id', array(':id'=>$id));

			$transaction->commit();
			Yii::app()->user->setFlash('success', "Evento Creado Correctamente");
			$this->redirect(array('site/index'));
		}
		catch (Exception $e)
		{
			$transaction->rollBack();
			Yii::app()->user->setFlash('danger', "{$e->getMessage()}");
			//$this->refresh();
		} 
	}

	$this->render('create',array(
		'model'=>$model,
		'modelOtros'=>$modelOtros
		));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
	$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

	if(isset($_POST['Eventoadv']))
	{
		$model->attributes=$_POST['Eventoadv'];
		if($model->save())
			$this->redirect(array('view','id'=>$model->id));
	}

	$this->render('update',array(
		'model'=>$model,
		));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
	if(Yii::app()->request->isPostRequest)
	{
// we only allow deletion via POST request
		$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
	$dataProvider=new CActiveDataProvider('Eventoadv');
	$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
	$model=new Eventoadv('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Eventoadv']))
	$model->attributes=$_GET['Eventoadv'];

$this->render('admin',array(
	'model'=>$model,
	));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
	$model=Eventoadv::model()->findByPk($id);
	if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
	return $model;
}

public function loadModelTmp($id)
{
	$model=Eventoadvtmp::model()->findByPk($id);
	if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
	return $model;
}

public function loadModelOtros($id)
{
	$model=Otros::model()->findByAttributes(array('id_eventoadv' => $id));
	if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
	return $model;
}

public function loadModelOtrosTmp($id)
{
	$model=Otrostmp::model()->findByAttributes(array('id_eventoadv' => $id));
	if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
	return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
	if(isset($_POST['ajax']) && $_POST['ajax']==='eventoadv-form')
	{
		echo CActiveForm::validate($model);
		Yii::app()->end();
	}
}
}
