<?php

/**
 * This is the model class for table "otrostmp".
 *
 * The followings are the available columns in table 'otrostmp':
 * @property integer $id
 * @property integer $id_eventoadv
 * @property string $dependencia
 * @property string $reportante
 * @property string $lugar
 * @property string $Incidente
 * @property string $tipoevento
 * @property string $transferido
 * @property string $facambiente
 * @property string $facadmon
 * @property string $facrh
 * @property string $factec
 * @property string $facarea
 * @property string $facpac
 *
 * The followings are the available model relations:
 * @property Eventoadvtmp $idEventoadv
 */
class Otrostmp extends CActiveRecord
{

	public $dependencia = '';
	public $reportante = '';
	public $lugar = '';
	public $Incidente = '';
	public $tipoevento = '';
	public $transferido = '';
	public $facambiente = '';
	public $facadmon = '';
	public $facrh = '';
	public $factec = '';
	public $facarea = '';
	public $facpac = '';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'otrostmp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_eventoadv', 'required'),
			array('id_eventoadv', 'numerical', 'integerOnly'=>true),
			array('dependencia, reportante, lugar, Incidente, tipoevento, transferido, facambiente, facadmon, facrh, factec, facarea, facpac', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_eventoadv, dependencia, reportante, lugar, Incidente, tipoevento, transferido, facambiente, facadmon, facrh, factec, facarea, facpac', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idEventoadvtmp' => array(self::BELONGS_TO, 'Eventoadvtmp', 'id_eventoadv'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_eventoadv' => 'Id Eventoadv',
			'dependencia' => 'Dependencia',
			'reportante' => 'Reportante',
			'lugar' => 'Lugar',
			'Incidente' => 'Incidente',
			'tipoevento' => 'Tipoevento',
			'transferido' => 'Transferido',
			'facambiente' => 'Facambiente',
			'facadmon' => 'Facadmon',
			'facrh' => 'Facrh',
			'factec' => 'Factec',
			'facarea' => 'Facarea',
			'facpac' => 'Facpac',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_eventoadv',$this->id_eventoadv);
		$criteria->compare('dependencia',$this->dependencia,true);
		$criteria->compare('reportante',$this->reportante,true);
		$criteria->compare('lugar',$this->lugar,true);
		$criteria->compare('Incidente',$this->Incidente,true);
		$criteria->compare('tipoevento',$this->tipoevento,true);
		$criteria->compare('transferido',$this->transferido,true);
		$criteria->compare('facambiente',$this->facambiente,true);
		$criteria->compare('facadmon',$this->facadmon,true);
		$criteria->compare('facrh',$this->facrh,true);
		$criteria->compare('factec',$this->factec,true);
		$criteria->compare('facarea',$this->facarea,true);
		$criteria->compare('facpac',$this->facpac,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Otrostmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
