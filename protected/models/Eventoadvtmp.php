<?php

/**
 * This is the model class for table "eventoadvtmp".
 *
 * The followings are the available columns in table 'eventoadvtmp':
 * @property integer $id
 * @property string $historia
 * @property integer $edad
 * @property string $fechareporte
 * @property string $fechaocurrencia
 * @property string $hora
 * @property integer $iddependencia
 * @property integer $idreportante
 * @property integer $idlugaro
 * @property integer $idtipoevento
 * @property integer $idincidente
 * @property integer $idtipoeventoadv
 * @property integer $idtransferido
 * @property integer $idfactoresmedioambiente
 * @property integer $idfactoresadmon
 * @property integer $idfactoresrh
 * @property integer $idfactorestec
 * @property integer $idfactortarea
 * @property integer $idfactorespaciente
 * @property integer $gestionado
 * @property string $observaciones
 * @property integer $idclasificacion
 * @property integer $estadot
 *
 * The followings are the available model relations:
 * @property Edad $edad0
 * @property Dependencia $iddependencia0
 * @property Factoresrh $idfactoresrh0
 * @property Factorestec $idfactorestec0
 * @property Factorestarea $idfactortarea0
 * @property Factorespaciente $idfactorespaciente0
 * @property Gestionado $gestionado0
 * @property Clasificacion $idclasificacion0
 * @property Tiporeportante $idreportante0
 * @property Lugaro $idlugaro0
 * @property Tipoevento $idtipoevento0
 * @property Incidente $idincidente0
 * @property Tipoeventoadv $idtipoeventoadv0
 * @property Transferido $idtransferido0
 * @property Factoresmedioambiente $idfactoresmedioambiente0
 * @property Factorestramitesadmon $idfactoresadmon0
 */
class Eventoadvtmp extends CActiveRecord
{
	public $idincidente = 0;
	public $idtipoeventoadv = 0;	
	public $idfactoresmedioambiente = 0;
	public $idfactoresadmon = 0;
	public $idfactoresrh = 0;
	public $idfactorestec = 0;
	public $idfactortarea = 0;
	public $idfactorespaciente = 0;
	public $gestionado = 2;
	public $idclasificacion = 0;

	public $dependencia_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'eventoadvtmp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('historia, edad, fechaocurrencia, hora, iddependencia, idreportante, idlugaro, idtipoevento, idincidente, idtipoeventoadv, idfactoresmedioambiente, idfactoresadmon, idfactoresrh, idfactorestec, idfactortarea, idfactorespaciente, gestionado, observaciones, idclasificacion', 'required'),
			array('edad, iddependencia, idreportante, idlugaro, idtipoevento, idincidente, idtipoeventoadv, idtransferido, idfactoresmedioambiente, idfactoresadmon, idfactoresrh, idfactorestec, idfactortarea, idfactorespaciente, gestionado, idclasificacion, estadot', 'numerical', 'integerOnly'=>true),
			array('historia', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, historia, edad, fechareporte, fechaocurrencia, hora, iddependencia, idreportante, idlugaro, idtipoevento, idincidente, idtipoeventoadv, idtransferido, idfactoresmedioambiente, idfactoresadmon, idfactoresrh, idfactorestec, idfactortarea, idfactorespaciente, gestionado, observaciones, idclasificacion, estadot, dependencia_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'edad0' => array(self::BELONGS_TO, 'Edad', 'edad'),
			'iddependencia0' => array(self::BELONGS_TO, 'Dependencia', 'iddependencia'),
			'idfactoresrh0' => array(self::BELONGS_TO, 'Factoresrh', 'idfactoresrh'),
			'idfactorestec0' => array(self::BELONGS_TO, 'Factorestec', 'idfactorestec'),
			'idfactortarea0' => array(self::BELONGS_TO, 'Factorestarea', 'idfactortarea'),
			'idfactorespaciente0' => array(self::BELONGS_TO, 'Factorespaciente', 'idfactorespaciente'),
			'gestionado0' => array(self::BELONGS_TO, 'Gestionado', 'gestionado'),
			'idclasificacion0' => array(self::BELONGS_TO, 'Clasificacion', 'idclasificacion'),
			'idreportante0' => array(self::BELONGS_TO, 'Tiporeportante', 'idreportante'),
			'idlugaro0' => array(self::BELONGS_TO, 'Lugaro', 'idlugaro'),
			'idtipoevento0' => array(self::BELONGS_TO, 'Tipoevento', 'idtipoevento'),
			'idincidente0' => array(self::BELONGS_TO, 'Incidente', 'idincidente'),
			'idtipoeventoadv0' => array(self::BELONGS_TO, 'Tipoeventoadv', 'idtipoeventoadv'),
			'idtransferido0' => array(self::BELONGS_TO, 'Transferido', 'idtransferido'),
			'idfactoresmedioambiente0' => array(self::BELONGS_TO, 'Factoresmedioambiente', 'idfactoresmedioambiente'),
			'idfactoresadmon0' => array(self::BELONGS_TO, 'Factorestramitesadmon', 'idfactoresadmon'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'historia' => 'Numero de Cédula',
			'edad' => 'Edad',
			'fechareporte' => 'Fechareporte',
			'fechaocurrencia' => 'Fecha de Ocurrencia',
			'hora' => 'Hora de Ocurrencia',
			'iddependencia' => 'Dependencia',
			'idreportante' => 'Reportante',
			'idlugaro' => 'Lugar',
			'idtipoevento' => 'Tipo de Evento',
			'idincidente' => 'Incidente',
			'idtipoeventoadv' => 'Tipo de Evento Adverso',
			'idtransferido' => 'Idtransferido',
			'idfactoresmedioambiente' => 'Factores Medio Ambiente',
			'idfactoresadmon' => 'Factores Administrativos',
			'idfactoresrh' => 'Factores de Recurso Humano',
			'idfactorestec' => 'Factores Tecnologia',
			'idfactortarea' => 'Factores Tarea',
			'idfactorespaciente' => 'Factores del Paciente',
			'gestionado' => 'Gestionado',
			'observaciones' => 'Descripcion del Caso',
			'idclasificacion' => 'Clasificacion del Evento Prevenible',
			'estadot' => 'Estadot',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array( 'iddependencia0' );

		$criteria->compare('id',$this->id);
		$criteria->compare('historia',$this->historia,true);
		$criteria->compare('edad',$this->edad);
		$criteria->compare('fechareporte',$this->fechareporte,true);
		$criteria->compare('fechaocurrencia',$this->fechaocurrencia,true);
		$criteria->compare('hora',$this->hora,true);
		$criteria->compare('iddependencia',$this->iddependencia);
		$criteria->compare('idreportante',$this->idreportante);
		$criteria->compare('idlugaro',$this->idlugaro);
		$criteria->compare('idtipoevento',$this->idtipoevento);
		$criteria->compare('idincidente',$this->idincidente);
		$criteria->compare('idtipoeventoadv',$this->idtipoeventoadv);
		$criteria->compare('idtransferido',$this->idtransferido);
		$criteria->compare('idfactoresmedioambiente',$this->idfactoresmedioambiente);
		$criteria->compare('idfactoresadmon',$this->idfactoresadmon);
		$criteria->compare('idfactoresrh',$this->idfactoresrh);
		$criteria->compare('idfactorestec',$this->idfactorestec);
		$criteria->compare('idfactortarea',$this->idfactortarea);
		$criteria->compare('idfactorespaciente',$this->idfactorespaciente);
		$criteria->compare('gestionado',$this->gestionado);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('idclasificacion',$this->idclasificacion);
		$criteria->compare('estadot',$this->estadot);

		$criteria->compare( 'iddependencia0.dependencia', $this->dependencia_search, true );


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
		        'attributes'=>array(
		            'dependencia_search'=>array(
		                'asc'=>'iddependencia0.dependencia',
		                'desc'=>'iddependencia0.dependencia DESC',
		            ),
		            '*',
		        ),
		    ),
		));
	}

	public function search2()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array( 'iddependencia0' );
		$criteria->condition = "estadot IS NULL";

		$criteria->compare('id',$this->id);
		$criteria->compare('historia',$this->historia,true);
		$criteria->compare('edad',$this->edad);
		$criteria->compare('fechareporte',$this->fechareporte,true);
		$criteria->compare('fechaocurrencia',$this->fechaocurrencia,true);

		$criteria->compare( 'iddependencia0.dependencia', $this->dependencia_search, true );


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
		        'attributes'=>array(
		            'dependencia_search'=>array(
		                'asc'=>'iddependencia0.dependencia',
		                'desc'=>'iddependencia0.dependencia DESC',
		            ),
		            '*',
		        ),
		    ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Eventoadvtmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
