<?php

/**
 * This is the model class for table "factoresmedioambiente".
 *
 * The followings are the available columns in table 'factoresmedioambiente':
 * @property integer $id
 * @property string $factoresmedioambiente
 *
 * The followings are the available model relations:
 * @property Eventoadv[] $eventoadvs
 * @property Eventoadvtmp[] $eventoadvtmps
 */
class Factoresmedioambiente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'factoresmedioambiente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('factoresmedioambiente', 'required'),
			array('factoresmedioambiente', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, factoresmedioambiente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'eventoadvs' => array(self::HAS_MANY, 'Eventoadv', 'idfactoresmedioambiente'),
			'eventoadvtmps' => array(self::HAS_MANY, 'Eventoadvtmp', 'idfactoresmedioambiente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'factoresmedioambiente' => 'Factores Medio Ambiente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('factoresmedioambiente',$this->factoresmedioambiente,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Factoresmedioambiente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
