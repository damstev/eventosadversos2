$(function(){

	var idTipoEventoAdversoPrevenible = 2;
	var optionOtros = {
		'iddependencia': 26,
		'idreportante': 15,
		'idlugaro':70,
		'idincidente': 19,
		'idtipoeventoadv': 56,
		'idfactoresmedioambiente': 8,
		'idfactoresadmon': 5,
		'idfactoresrh': 10,
		'idfactorestec': 17,
		'idfactortarea': 10,
		'idfactorespaciente': 3
	}

	// [
	// 	'idclasificacion',
	// 	'idincidente',
	// 	'idtipoeventoadv',
	// 	'idfactoresmedioambiente',
	// 	'idfactoresadmon',
	// 	'idfactoresrh',
	// 	'idfactorestec',
	// 	'idfactortarea',
	// 	'idfactorespaciente',
	// ]

	var tipo_evento_combo_dependencias = {
		'1' : [
			'idincidente',
			'idfactoresmedioambiente',
			'idfactoresadmon',
			'idfactoresrh',
			'idfactorestec',
			'idfactortarea',
			'idfactorespaciente'
		],
		'2' : [
			'idclasificacion',
			'idtipoeventoadv',
			'idfactoresmedioambiente',
			'idfactoresadmon',
			'idfactoresrh',
			'idfactorestec',
			'idfactortarea',
			'idfactorespaciente',
		],
		'3' : [],
		'4' : [],
		'5' : []
	}

	$('#Eventoadvtmp_idtipoevento').change(deshabilitar_campos_segun_tipo_evento);

	$('.trigger_otros').change(revisar_campo_otros);



	// function deshabilitar_campos_prevenibles(){
	// 	if($('#Eventoadvtmp_idtipoevento').val() == idTipoEventoAdversoPrevenible)
	// 		$('.campo_prevenible').attr('disabled', false);
	// 	else
	// 		$('.campo_prevenible').attr('disabled', true);
	// }

	function deshabilitar_campos_segun_tipo_evento(){
		var tipoEvento = $('#Eventoadvtmp_idtipoevento').val();
		var campos_a_habilitar = tipo_evento_combo_dependencias[tipoEvento];
		// console.log('campos_a_habilitar', campos_a_habilitar);
		$('.combo_dependiente').attr('disabled', true);
		for(var i=0; i<campos_a_habilitar.length; i++){

			var select_id = "#Eventoadvtmp_"+campos_a_habilitar[i];
			// console.log('select_id', select_id);
			$(select_id).attr('disabled', false);
		}
	}

	function revisar_campo_otros(){
		for(var id in optionOtros){
			var value = optionOtros[id];
			if($('#Eventoadvtmp_'+id).val() == value){
				$('#otros_'+id).parent().parent().show('slow');
			}else{
				$('#otros_'+id).val('').parent().parent().hide('slow');
			}

		}
	}

	function deshabilitar_lugares_mayusculas(){
		$('#Eventoadvtmp_idlugaro option').each(function(){
			var text = $(this).html();
			if(text === text.toUpperCase())
				$(this).attr('disabled', true);
			//console.log($(this).val() + ' - '+$(this).html())
		});
	}

	function deshabilitar_tipos_eventos_mayusculas(){
		$('#Eventoadvtmp_idtipoeventoadv option').each(function(){
			var text = $(this).html();
			if(text === text.toUpperCase())
				$(this).attr('disabled', true);
			//console.log($(this).val() + ' - '+$(this).html())
		});
	}


	deshabilitar_campos_segun_tipo_evento();
	revisar_campo_otros();
	deshabilitar_lugares_mayusculas();
	deshabilitar_tipos_eventos_mayusculas();
});
